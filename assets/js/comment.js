$.getScript('https://www.gstatic.com/firebasejs/3.4.0/firebase.js', function () {
    var config = {
        apiKey: "AIzaSyDHzOKs9z0Ciqm4ToOTIk1nhm_anAHz_IY",
        authDomain: "comments-keralapsc.firebaseapp.com",
        databaseURL: "https://comments-keralapsc.firebaseio.com",
        projectId: "comments-keralapsc",
        storageBucket: "comments-keralapsc.appspot.com",
        messagingSenderId: "603281439612",
        appId: "1:603281439612:web:6442fda1da5f2db72782f9",
        measurementId: "G-3N0LZSFDKG"
    };
    firebase.initializeApp(config);
    var rootRef = firebase.database().ref();
    var postComments = rootRef.child('postComments');
    var link = $("link[rel='canonical']").attr("href");
    var pathkey = decodeURI(link.replace(new RegExp('\\/|\\.', 'g'),"_"));
    var postRef = postComments.child(pathkey);

    $("#comment").submit(function() {
        postRef.push().set({            
            name: $("#name").val(),
            message: $("#message").val(),
            md5Email: md5($("#email").val()),
            postedAt: firebase.database.ServerValue.TIMESTAMP
        });
   
        $("input[type=text], textarea").val("");
        return false;
    });


    postRef.on("child_added", function(snapshot) {
        var newComment = snapshot.val();
        var converter = new showdown.Converter({ extensions: ['xssfilter'] });
        converter.setFlavor('github');
        var markedMessage = converter.makeHtml(newComment.message);
        var html = "<div class='comment'>";
        html += "<h4>" + newComment.name + "</h4>";
        html += "<div class='profile-image'><img src='https://www.gravatar.com/avatar/" + newComment.md5Email + "?s=100&d=retro'/></div>";
        html += "<span class='date'>" + jQuery.timeago(newComment.postedAt) + "</span>"
        html += "<p>" + markedMessage  + "</p></div>";
        $("#comments-container").prepend(html);
    });
})
