const staticCacheName = 'site-static-v2';
const dynamicCacheName = 'site-dynamic-v1';
const assets = [
  'https://keralapsc.monsoonmalabar.com/',
  'https://keralapsc.monsoonmalabar.com/index.html',
  'https://keralapsc.monsoonmalabar.com/assets/css/all.css',
  'https://keralapsc.monsoonmalabar.com/assets/css/bootstrap.min.css',
  'https://keralapsc.monsoonmalabar.com/assets/css/mdb.min.css',
  'https://keralapsc.monsoonmalabar.com/assets/css/style.css',
  'https://keralapsc.monsoonmalabar.com/assets/css/animate.css',
  'https://keralapsc.monsoonmalabar.com/assets/js/jquery-3.4.1.min.js',
  'https://keralapsc.monsoonmalabar.com/assets/js/popper.min.js',
  'https://keralapsc.monsoonmalabar.com/assets/js/bootstrap.min.js',
  'https://keralapsc.monsoonmalabar.com/assets/js/mdb.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/blueimp-md5/2.7.0/js/md5.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.5.4/jquery.timeago.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/showdown/1.6.4/showdown.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/js-xss/0.3.3/xss.min.js',
  'https://keralapsc.monsoonmalabar.com/assets/js/showdown-xss-filter.js',
  'https://keralapsc.monsoonmalabar.com/assets/js/comment.js',
  'https://keralapsc.monsoonmalabar.com/assets/images/logo.png',
  'https://keralapsc.monsoonmalabar.com/pages/fallback/index.html'
];

// cache size limit function
const limitCacheSize = (name, size) => {
  caches.open(name).then(cache => {
    cache.keys().then(keys => {
      if(keys.length > size){
        cache.delete(keys[0]).then(limitCacheSize(name, size));
      }
    });
  });
};

// install event
self.addEventListener('install', evt => {
  //console.log('service worker installed');
  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      console.log('caching shell assets');
      cache.addAll(assets);
    })
  );
});

// activate event
self.addEventListener('activate', evt => {
  //console.log('service worker activated');
  evt.waitUntil(
    caches.keys().then(keys => {
      //console.log(keys);
      return Promise.all(keys
        .filter(key => key !== staticCacheName && key !== dynamicCacheName)
        .map(key => caches.delete(key))
      );
    })
  );
});

// fetch event
self.addEventListener('fetch', evt => {
  //console.log('fetch event', evt);
  evt.respondWith(
    caches.match(evt.request).then(cacheRes => {
      return cacheRes || fetch(evt.request).then(fetchRes => {
        return caches.open(dynamicCacheName).then(cache => {
          cache.put(evt.request.url, fetchRes.clone());
          // check cached items size
          limitCacheSize(dynamicCacheName, 2500);
          return fetchRes;
        })
      });
    }).catch(() => {
      return caches.match('https://keralapsc.monsoonmalabar.com/pages/fallback/index.html');
    })
  );
});